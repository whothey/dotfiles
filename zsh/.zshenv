# Environment Setup

# .oh-my-zsh default
export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl"

# My PATH Updates
export PATH="$PATH:$HOME/.gem/ruby/2.3.0/bin:$HOME/.composer/vendor/bin:$HOME/.cargo/bin"

export RUST_SRC_PATH="$HOME/Repositories/rust/src"
export CARGO_HOME="$HOME/.cargo"

export BROWSER=/usr/bin/firefox
export TERMINAL=/usr/bin/urxvtc
