# Dotfiles

These funny little files live across my Home directory, and then I've realized
that sometimes them can simply goes \*PUF\* by some imp hidden on the
filesystem, and sometimes I just want to share these across machines (work/home).

## My setup 

I'm currently sharing these files between Antegos/Fedora systems, and using some
standard apps across them:

| Function       | Primary             | Secondary      |
| -------------- | ------------------- | -------------- |
| Window Manager | i3                  | Gnome          |
| Terminal       | urxvt               | Gnome Terminal |
| Shell          | zsh + oh-my-zsh     | Bash           |
| Editor         | Emacs(Spacemacs)    | VIM (spf13)    |
| Web Browser    | Firefox(Vimperator) | Chrome         |
| File explorer  | ranger              | Nautilus       |
| PDF Viewer     | zathura             | --             |

~~I've actualy listed these to show about some dotfiles~~
I actualy don't know why I've made this table.

## Bootstrapping

The files in this repository are organized as GNU Stow Packages.
